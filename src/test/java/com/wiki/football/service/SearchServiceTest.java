package com.wiki.football.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static com.wiki.football.helper.FileHelper.readFile;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTest {

    @InjectMocks
    private SearchService searchService;
    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setUp() {
        searchService = new SearchService(restTemplate);
    }

    @Test
    public void shouldFetchClubInformationByName() {
        //given
        ResponseEntity<String> spy = Mockito.spy(new ResponseEntity<>(readFile("liverpool"), HttpStatus.OK));
        doReturn(spy).when(restTemplate).getForEntity(
                any(String.class),
                any(Class.class));

        //when
        ResponseEntity<String> randomCityName = searchService.fetchClubInformationByName("liverpool");

        //then
        assertNotNull(randomCityName);
        assertThat(randomCityName.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(randomCityName.getBody()).isNotEmpty();
        assertThat(randomCityName.getBody()).contains("football");
        assertThat(randomCityName.getBody()).contains("club");
    }
}