package com.wiki.football.controller;

import com.wiki.football.exception.ClubNotFoundException;
import com.wiki.football.service.SearchService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {SearchController.class})
class SearchControllerTest {

    public static final String CITY_NAME = "random";
    @MockBean
    private SearchService searchService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldFetchClubInformationByNameAndReturnHttpOk() throws Exception {
        //given
        ResponseEntity<String> clubInformation = new ResponseEntity<>("clubInformation", HttpStatus.OK);
        when(searchService.fetchClubInformationByName(CITY_NAME))
                .thenReturn(clubInformation);

        //when
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/query")
                .param("cityName", CITY_NAME);
        ResultActions resultActions = mockMvc.perform(requestBuilder);
        //then
        resultActions.andExpect(status().isOk());
        resultActions.andExpect(content().string(clubInformation.getBody()));

    }

    @Test
    public void shouldNotReturnFetchClubInformationByNameAndReturnHttpNotFound() throws Exception {
        //given
        when(searchService.fetchClubInformationByName(CITY_NAME))
                .thenThrow(ClubNotFoundException.class);

        //when
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/query")
                .param("cityName", CITY_NAME);
        ResultActions resultActions = mockMvc.perform(requestBuilder);
        //then
        resultActions.andExpect(status().isNotFound());

    }
}