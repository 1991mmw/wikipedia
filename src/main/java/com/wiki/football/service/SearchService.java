package com.wiki.football.service;

import com.google.gson.Gson;
import com.wiki.football.exception.ClubNotFoundException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toCollection;

@Service
public class SearchService {

    private RestTemplate restTemplate;

    SearchService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<String> fetchClubInformationByName(String cityName) {
        String url = "https://en.wikipedia.org/w/api.php?action=" +
                "query&list=search&srsearch=" + cityName.toLowerCase() + "&srlimit=10&format=json";
        ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);

        JSONArray search = parseResponse(forEntity);
        List<HashMap> listOfHashMaps = fetchHashMapList(search);
        HashMap hashMap = extractClubPage(listOfHashMaps, cityName);

        return generateResponseEntity(hashMap);
    }

    private ResponseEntity<String> generateResponseEntity(HashMap hashMap) {
        String title = (String) hashMap.get("title");
        title = title.replaceAll(" ", "_");
        String url = "https://en.wikipedia.org/wiki/" + title;

        return restTemplate.getForEntity(url, String.class);
    }

    private HashMap extractClubPage(List<HashMap> listOfHashMaps, String cityName) {
        for (HashMap hashMap : listOfHashMaps) {
            String snippet = (String) hashMap.get("snippet");
            if (snippet.contains("football") || snippet.contains("club") && !snippet.contains("season")) {
                return hashMap;
            }
        }
        throw new ClubNotFoundException(cityName);
    }

    private List<HashMap> fetchHashMapList(JSONArray search) {
        ArrayList list = IntStream.range(0, search.length())
                .mapToObj(i -> (JSONObject) search.get(i))
                .collect(toCollection((Supplier<ArrayList>) ArrayList::new));

        return IntStream.range(0, list.size())
                .mapToObj(i -> (JSONObject) list.get(i))
                .map(jsonObject -> new Gson().fromJson(jsonObject.toString(), HashMap.class))
                .collect(Collectors.toList());
    }

    private JSONArray parseResponse(ResponseEntity<String> forEntity) {
        String body = forEntity.getBody();
        JSONObject obj = new JSONObject(body);
        JSONObject query = obj.getJSONObject("query");
        return query.getJSONArray("search");
    }

}
