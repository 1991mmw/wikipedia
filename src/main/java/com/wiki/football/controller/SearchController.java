package com.wiki.football.controller;

import com.wiki.football.service.SearchService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController {

    private SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/query")
    public ResponseEntity<String> fetchClubInformationByName(@RequestParam(name = "cityName") String cityName) {
        return searchService.fetchClubInformationByName(cityName);
    }
}
