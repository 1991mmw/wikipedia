package com.wiki.football.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ClubNotFoundException extends RuntimeException {

    public ClubNotFoundException() {
        super();
    }

    public ClubNotFoundException(String cityName) {
        super(String.format("Can't find club page for given city: [%s]", cityName));
    }
}
